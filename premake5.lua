project "spdlog"
kind "StaticLib"
language "C++"

targetdir ("bin/" .. outputdir .. "/%{prj.name}")
objdir ("bin-obj/" .. outputdir .. "/%{prj.name}")

files {
    "include/**.h",
    "include/**.hpp",
    "src/**.c",
    "src/**.cpp"
}

includedirs {
    "include",
}

defines { 
    "SPDLOG_COMPILED_LIB",
}

filter "system:windows"
    systemversion "latest"
    cppdialect "C++20"
    staticruntime "On"

filter "configurations:Debug"
    runtime "Debug"
    symbols "on"

filter "configurations:Release"
    runtime "Release"
    optimize "on"
